<?php
// Configuration
$hostname = 'localhost';
$username = 'root';
$password = '';
$database = 'game';

try {
    $dbh = new PDO('mysql:host='. $hostname .';dbname='. $database, $username, $password);
} catch(PDOException $e) {
    echo '<h1>An error has occurred.</h1><pre>', $e->getMessage() ,'</pre>';
}
// $sth = $dbh->query("SELECT * FROM people WHERE name LIKE '%$keyword%' OR building LIKE '%$keyword%' OR floor LIKE '%$keyword%' OR room LIKE '%$keyword%'");
$sth = $dbh->query("SELECT * FROM users");
$sth->setFetchMode(PDO::FETCH_ASSOC);

$result = $sth->fetchAll();

foreach($result as $r) {
    // echo mysql_fetch_lengths($result);
    echo $r['email'], ";", $r['passWord'], ";", $r['fullName'],"<br/>";
}?>