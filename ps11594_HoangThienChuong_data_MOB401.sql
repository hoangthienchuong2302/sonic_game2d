-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 20, 2021 lúc 01:12 PM
-- Phiên bản máy phục vụ: 10.4.18-MariaDB
-- Phiên bản PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `game`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `email` varchar(100) NOT NULL,
  `passWord` text NOT NULL,
  `fullName` text NOT NULL,
  `age` int(11) NOT NULL,
  `gold` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`email`, `passWord`, `fullName`, `age`, `gold`) VALUES
('a', '1', 'a', 0, 100),
('ad', '123', 't', 20, 195),
('addd', '123', 'r', 6, 2),
('An@gmail.com', '123', 'Trường An', 19, 0),
('chuong@gmail.com', '234', 'Thiên Chương', 24, 10),
('chuonghtps11594@gmail.com', '2345', 'Chương', 24, 0),
('chuonghtps@gmail.com', '234', 'Chương', 24, 0),
('Cuong@gmail.com', '123', 'Nguyễn Tấn Cường', 19, 0),
('df', '1', 'f', 2, 2),
('Khang@gmail.com', '123', 'Duy Khang', 19, 0),
('Nam@gmail.com', '123', 'Thành Nam', 19, 0),
('phong@gmail.com', '123', 'Dương Tấn Nhật Phong', 19, 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
