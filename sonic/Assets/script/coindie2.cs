﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coindie2 : MonoBehaviour
{
        public Animator myAnim;
    // Start is called before the first frame update
    Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        myAnim.SetBool("Collect", true);
        myAnim.Play("collect");
         StartCoroutine(Pickuped());   
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Pickuped()
    {
        yield return new WaitForSeconds(0.4f);
        Destroy(gameObject);
    }
}
