using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Login : MonoBehaviour
{
    // Start is called before the first frame update
    public InputField email;
    public InputField pass;


    string loginPHP = "http://localhost/PHP/login.php";
    public void Log()
    {
        StartCoroutine(connect());
    }
    IEnumerator connect()
    {
        Debug.Log("email" + email.text + "" + pass.text);
        WWWForm wf = new WWWForm();
        wf.AddField("email_change", email.text);
        wf.AddField("pass_change", pass.text);

        WWW w = new WWW(loginPHP, wf);
        yield return w;

        string cutEmpty = w.text;
        string cutEmpty1 = cutEmpty.TrimStart();
        string cutEmpty2 = cutEmpty1.TrimEnd();
        if (w.text.Contains("đúng"))
        {
            print("Đăng nhập thành công");
            staticInf.email = email.text;

            SceneManager.LoadScene("Start");
        }
        else
        {
            print("Đăng nhập không thành công");
        }
    }
}
