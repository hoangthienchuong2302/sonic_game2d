﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    // Start is called before the first frame update
   public int faceVal = -1;

    Rigidbody2D myBody;
    public Animator myAnim;
    public bool Die = false;


    // Start is called before the first frame update
    void Awake()
    {
        myBody = gameObject.GetComponent<Rigidbody2D>();
        myAnim = gameObject.GetComponent<Animator>();
        // 
        if (!Die)
            StartCoroutine(PrintfAfter(4));
    }

    // Update is called once per frame
    void Update()
    {
        if (!Die)
            transform.Translate(-Time.deltaTime * 8 * faceVal, 0, 0);
    }
    IEnumerator PrintfAfter(float seconds)
    {

        yield return new WaitForSeconds(seconds);
        faceVal = faceVal * -1;
        flip();
        StartCoroutine(PrintfAfter(4));
    }
    void OnTriggerEnter2D(Collider2D Colli)
    {
        if (Colli.gameObject.tag == "FireBall")
        {
            myAnim.SetBool("Die_Monster", true);
            myAnim.Play("Monster_Die");
            transform.Translate(0, 0, 0);
            StartCoroutine(DieAfter(0.5f));
        }

    }
        IEnumerator DieAfter(float seconds)
    {

        yield return new WaitForSeconds(seconds);
        Destroy(GameObject.Find("Monster"));
    }

    void flip()
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
