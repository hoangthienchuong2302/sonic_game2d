﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SonicDie : MonoBehaviour
{
    public BoxCollider2D bc;
    public Animator myAnim;
    public Animator myAnim2;
    public GameObject panelDie;
    public int count;
    // Start is called before the first frame update
    void Start()
    {
        count = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D Colli)
    {

        if (Colli.gameObject.tag == "Dragon_Down" || Colli.gameObject.tag == "Boss" || Colli.gameObject.tag == "Fire_Boss" || Colli.gameObject.tag == "Monser" || Colli.gameObject.tag == "rock")
        {
            count++;
            Debug.Log(count);
            bc.enabled = false;
            StartCoroutine(EnabledAfter(1f));
        }

        if (Colli.gameObject.tag == "water")
        {
            count = 6;
        }
        if (count == 6)
        {
            myAnim.SetBool("sonic_die", true);
            myAnim.Play("sonic_die");
            StartCoroutine(PrintfAfter(0.1f));
            AudioGameOver.playMusic();
        }
    }
    IEnumerator PrintfAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Destroy(GameObject.Find("sonic"));
        panelDie.SetActive(true);
    }
    IEnumerator EnabledAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        bc.enabled = true;
    }
}
