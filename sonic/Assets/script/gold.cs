﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
public class gold : MonoBehaviour
{
    public static int txtGold = 0;
    public Text goldTest;
    string url = "http://localhost/php/data.php";
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(connect());

    }

    // Update is called once per frame
    void Update()
    {
        goldTest.text = "" + txtGold;
    }
    IEnumerator connect()
    {
        WWWForm wf = new WWWForm();
        wf.AddField("email", staticInf.email);

        WWW w = new WWW(url, wf);
        yield return w;

        string cutEmpty = w.text;
        string cutEmpty1 = cutEmpty.TrimStart();
        string cutEmpty2 = cutEmpty1.TrimEnd();
        Debug.Log(w.text);
        txtGold = Int32.Parse(w.text);

    }
}
