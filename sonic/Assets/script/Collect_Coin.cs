﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collect_Coin : MonoBehaviour
{
    public GameObject pickupPrelab;
    public Add_Point add;
    Rigidbody2D myBody;

    CircleCollider2D cc;
    public Animator myAnim;
    public AudioSource sound;
    // Start is called before the first frame update
    void Start()
    {
        add = GameObject.FindWithTag("Point").GetComponent<Add_Point>();
        cc = GetComponent<CircleCollider2D>();
        myBody = GetComponent<Rigidbody2D>();
        myAnim = GetComponent<Animator>();
        sound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D Colli)
    {
        if (Colli.gameObject.tag != "FireBall" && Colli.gameObject.tag != "rock")
        {
            cc.enabled = false;
            // 
            add.UpdatePoint();
            // 
            Destroy(gameObject);

            Instantiate(pickupPrelab, transform.position, Quaternion.identity);
            AudioCoins.playMusic();
        }

    }

}
