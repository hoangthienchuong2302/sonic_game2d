﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class creatObjectRandom : MonoBehaviour
{
    public GameObject createObject;
    public GameObject startIndex;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(tao());

    }
    IEnumerator tao()
    {
        yield return new WaitForSeconds(2f);
        Vector3 temp = startIndex.transform.position;
        temp.x = Random.Range(50f, 0f);
        Instantiate(createObject, temp, Quaternion.identity);

        StartCoroutine(tao());
    }

    // Update is called once per frame
    void Update()
    {

    }
}
