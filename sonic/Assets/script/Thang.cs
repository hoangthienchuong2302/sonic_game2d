﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thang : MonoBehaviour
{
    public int faceVal = 1;

    Rigidbody2D myBody;
    public Animator myAnim;

    public bool Die = false;


    // Start is called before the first frame update
    void Awake()
    {
        myBody = gameObject.GetComponent<Rigidbody2D>();
        myAnim = gameObject.GetComponent<Animator>();
        StartCoroutine(PrintfAfter(6));
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, Time.deltaTime * 5 * faceVal, 0);
    }
    IEnumerator PrintfAfter(float seconds)
    {

        yield return new WaitForSeconds(seconds);
        faceVal = faceVal * -1;
        StartCoroutine(PrintfAfter(6));
    }
}
