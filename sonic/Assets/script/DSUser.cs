﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DSUser : MonoBehaviour
{
    public GameObject rownew;
    string URL = "http://localhost/php/showLeaderBoard.php";
    // Start is called before the first frame update
    void Start()
    {
        GetData();
    }
    public void GetData()
    {
        StartCoroutine(connect());
    }
    IEnumerator connect()
    {
        WWWForm wf = new WWWForm();
        WWW w = new WWW(URL, wf);

        yield return w;
        string data = w.text;
        string[] a = new string[] { };
        a = data.Split(';');
        for (int i = 0; i < (a.Length) - 1; i++)
        {
            string row = a[i];
            string[] b = new string[] { };
            b = row.Split(',');
            GameObject g = (GameObject)Instantiate(rownew);
            g.transform.SetParent(this.transform);
            g.transform.Find("Email").GetComponent<Text>().text = b[0];
            g.transform.Find("Name").GetComponent<Text>().text = b[1];
            g.transform.Find("Score").GetComponent<Text>().text = b[2];



        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}
