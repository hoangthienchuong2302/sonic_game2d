﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
public class shop : MonoBehaviour
{
    public static bool isBackground = false;
    public Text goldTest;
    public static int txtGold = 0;
    public GameObject showIsSkill;
    public Button btnShop;

    // Start is called before the first frame update

    string url = "http://localhost/php/buy.php";
    string urlData = "http://localhost/php/data.php";
    public static int gold = 0;
    public void ChangeScene(string scene_name)
    {
        SceneManager.LoadScene(scene_name);
    }
    public void Buy(int price)
    {
        showIsSkill.SetActive(false);
        if (staticInf.gold >= price)
        {
            btnShop.interactable = false;
            //được mua
            StartCoroutine(connect(price));
            StartCoroutine(goldUpdate());
        }
        else
        {
            //không đủ vàng
        }
    }
    public void Buy2()
    {
        isBackground = true;
    }
    IEnumerator connect(int price)
    {
        WWWForm wf = new WWWForm();
        wf.AddField("email", staticInf.email);
        wf.AddField("name", "1");
        wf.AddField("price", price);
        WWW w = new WWW(url, wf);
        yield return w;

        string cutEmpty = w.text;
        string cutEmpty1 = cutEmpty.TrimStart();
        string cutEmpty2 = cutEmpty1.TrimEnd();
        showIsSkill.SetActive(true);




    }
    IEnumerator goldUpdate()
    {
        {
            WWWForm wf = new WWWForm();
            wf.AddField("email", staticInf.email);

            WWW w = new WWW(urlData, wf);
            yield return w;

            string cutEmpty = w.text;
            string cutEmpty1 = cutEmpty.TrimStart();
            string cutEmpty2 = cutEmpty1.TrimEnd();
            Debug.Log(w.text);
            txtGold = Int32.Parse(w.text);
            goldTest.text = "" + txtGold;


        }
    }
    void Update()
    {
    }
    void Start()
    {
        goldTest.text = "" + staticInf.gold;
    }
}
