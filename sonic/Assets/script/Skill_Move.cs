using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_Move : MonoBehaviour
{
    Rigidbody2D rb;
    public Animator myAnim;
    public Animator myAnim2;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
        rb.velocity = transform.right * 40;
        StartCoroutine(PrintfAfter(0.5f));
    }

    // lay vi tri ban dau position.x
    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D Colli)
    {
        if(Colli.gameObject.tag == "Player"||Colli.gameObject.tag=="Coins"){
           
        }else if(Colli.gameObject.tag == "Dragon_Down"||Colli.gameObject.tag == "Dragon_Up"){
            Destroy(GameObject.Find("Dragon"));
            Destroy(gameObject);
        }else{
            Destroy(gameObject);
        }

    }
    IEnumerator PrintfAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Destroy(gameObject);
    }
}
