﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class pause : MonoBehaviour
{
    public GameObject pauseBoard;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseBoard.SetActive(true);
            Time.timeScale = 0;
        }
    }
    public void onclickCon()
    {
        pauseBoard.SetActive(false);
        Time.timeScale = 1;
    }
    public void changSen()
    {
        SceneManager.LoadScene("Start");
        Time.timeScale = 1;
    }
}
