﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HP : MonoBehaviour
{
    public Animator myAnim;
    public int count;
    // Start is called before the first frame update
    void Start()
    {
        count = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D Colli)
    {
        if (Colli.gameObject.tag == "Dragon_Down" || Colli.gameObject.tag == "Boss" || Colli.gameObject.tag == "Fire_Boss" || Colli.gameObject.tag == "Monser" || Colli.gameObject.tag == "rock")
        {
            count++;
            Debug.Log(count);
        }
        if (count == 1)
        {
            myAnim.SetBool("hit1", true);
            myAnim.Play("HP_5");
        }
        else if (count == 2)
        {
            myAnim.SetBool("hit2", true);
            myAnim.Play("HP_4");
        }
        else if (count == 3)
        {
            myAnim.SetBool("hit3", true);
            myAnim.Play("HP_3");
        }
        else if (count == 4)
        {
            myAnim.SetBool("hit4", true);
            myAnim.Play("HP_2");
        }
        else if (count == 5)
        {
            myAnim.SetBool("hit1", true);
            myAnim.Play("HP_1");
        }

    }
}
