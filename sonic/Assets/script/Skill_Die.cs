﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_Die : MonoBehaviour
{

    Rigidbody2D myBody;

    public Animator myAnim;
    public Animator myAnim2;

    // Start is called before the first frame update
    void Start()
    {

        myBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D Colli)
    {
        myAnim2.SetBool("hit", true);
        myAnim.SetBool("Skill_collect", true);
        myAnim.Play("Skill_collect");
        StartCoroutine(PrintfAfter(0.5f));
    }
    IEnumerator PrintfAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Destroy(gameObject);
    }
}
