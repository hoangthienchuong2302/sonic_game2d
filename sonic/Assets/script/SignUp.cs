﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class SignUp : MonoBehaviour
{
    // Start is called before the first frame update
    public InputField email;
    public InputField pass;
    public InputField fullName;
    public InputField age;

    string loginPHP = "http://localhost/PHP/signup.php";
    public void Log()
    {
        StartCoroutine(connect());
    }
    IEnumerator connect()
    {
        WWWForm wf = new WWWForm();
        wf.AddField("email_change", email.text);
        wf.AddField("pass_change", pass.text);
        wf.AddField("fullName_change", fullName.text);
        wf.AddField("age_change", age.text);


        WWW w = new WWW(loginPHP, wf);
        yield return w;

        string cutEmpty = w.text;
        string cutEmpty1 = cutEmpty.TrimStart();
        string cutEmpty2 = cutEmpty1.TrimEnd();
        if (w.text == "thành công")
        {
            print("Đăng nhập thành công");
            SceneManager.LoadScene("Login");
        }
        else
        {
            print("Đăng nhập không thành công");
        }
    }
}
