﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sonicControll : MonoBehaviour
{
    public float maxSpeed;
    public float jumpHeight;

    bool facingRight;
    public bool grounded = true;
    bool canbemove;
    bool isShop = false;
    //khai bao cac bien de nem kunai




    Rigidbody2D myBody;
    public Animator myAnim;
    // Use this for initialization
    void Start()
    {
        myBody = GetComponent<Rigidbody2D>();
        myAnim = GetComponent<Animator>();

        facingRight = true;
        canbemove = true;

    }

    // Update is called once per frame
    void FixedUpdate()

    {
        float move = Input.GetAxis("Horizontal");
        // myAnim.SetBool("hit", false);
        if (canbemove == true)
        {
            move = Input.GetAxis("Horizontal");
            myAnim.SetBool("grounded", grounded);
            myAnim.SetFloat("speed", Mathf.Abs(move));
            myBody.velocity = new Vector2(move * maxSpeed, myBody.velocity.y);

            if (move > 0 && !facingRight)
            {
                _flip(1);
            }
            else if (move < 0 && facingRight)
            {
                _flip(-1);
            }

        }
        if (grounded)
        {
            myAnim.SetBool("Jump", false);
            myAnim.SetBool("grounded", grounded);

            if (Input.GetKey(KeyCode.UpArrow))
            {
                AudioJump.playMusic();
                myAnim.SetBool("Jump", true);
                myBody.AddForce(Vector2.up * jumpHeight);
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (grounded == false)
                {
                    grounded = true;
                    if (Input.GetKeyDown(KeyCode.UpArrow))
                    {
                        if (grounded == true)
                        {
                            AudioJump.playMusic();
                            myAnim.SetBool("Jump", true);
                            grounded = false;
                        }
                    }
                }
                //Sang trái và nhảy
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (grounded == false)
                {
                    grounded = true;
                    if (Input.GetKeyDown(KeyCode.UpArrow))
                    {
                        if (grounded == true)
                        {
                            AudioJump.playMusic();
                            myAnim.SetBool("Jump", true);
                            grounded = false;
                        }
                    }
                }
                // Chạy và nhảy
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow) && Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (grounded == true)
                {

                    grounded = false;
                    myBody.AddForce(Vector2.up * jumpHeight);
                    myAnim.SetBool("Jump", true);
                    AudioJump.playMusic();
                }
                //Chảy và nhảy
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow) && Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (grounded == true)
                {
                    grounded = false;
                    myBody.AddForce(Vector2.up * jumpHeight);
                    myAnim.SetBool("Jump", true);
                    AudioJump.playMusic();
                }
            }
        }

    }


    void _flip(int val)
    {
        facingRight = !facingRight;
        transform.Rotate(Vector3.up * 180 * val);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "grounded")
        {
            grounded = true;
        }


    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "shop")
        {
            SceneManager.LoadScene("shop");

        }
    }


    public void setcanbemove(bool b)
    {
        canbemove = b;
    }
    //chuc nang nem phi tiêu
}
