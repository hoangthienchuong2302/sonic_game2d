﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Add_Point : MonoBehaviour
{
    public int count = 0;
    public Text txtPoint;

    void Start()
    {
        txtPoint.text = "Gold: " + staticInf.gold;
    }
    public void UpdatePoint()
    {
        staticInf.gold = staticInf.gold + 1;
        txtPoint.text = "Gold: " + staticInf.gold;
    }
}
