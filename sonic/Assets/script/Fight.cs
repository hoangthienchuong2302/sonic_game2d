﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fight : MonoBehaviour
{
    Rigidbody2D myBody;
    public Transform firePoint;

    public GameObject fireBall;

    public Animator myAnim;
    public Animator myAnim2;
    public bool isSkill = false;
    string cartPHP = "http://localhost/PHP/cart.php";
    void Start()
    {
        myBody = GetComponent<Rigidbody2D>();
        StartCoroutine(connect());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isSkill == true)
            {
                Debug.Log(firePoint.rotation);
                Instantiate(fireBall, firePoint.position, firePoint.rotation);
                AudioFireball.playMusic();
            }

        }
    }
    void OnTriggerEnter2D(Collider2D Colli)
    {
        if (Colli.gameObject.tag == "Dragon_Up")
        {
            myAnim.SetBool("Die", true);
            myAnim.Play("Dragon_die");
            StartCoroutine(PrintfAfter(0.3f));
        }


    }
    IEnumerator PrintfAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Destroy(GameObject.Find("Dragon"));
    }
    IEnumerator connect()
    {

        WWWForm wf = new WWWForm();
        wf.AddField("email", staticInf.email);

        WWW w = new WWW(cartPHP, wf);
        yield return w;

        string cutEmpty = w.text;
        string cutEmpty1 = cutEmpty.TrimStart();
        string cutEmpty2 = cutEmpty1.TrimEnd();
        if (w.text.Contains("đúng"))
        {
            Debug.Log("Có skill");
            print("Có skill");
            isSkill = true;
        }
        else
        {
            isSkill = false;
            print("Chưa mua skill");
        }
    }
}
