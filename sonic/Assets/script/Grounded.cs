﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grounded : MonoBehaviour
{
    public sonicControll sonic;
    // Start is called before the first frame update
    void Start()
    {
        sonic = gameObject.GetComponentInParent<sonicControll>();
    }

    void FixedUpdate(){

    }

    void OnTriggerEnter2D(Collider2D collision){
        sonic.grounded = true;
    }

    private void OnTriggerExit2D(Collider2D collision){
        sonic.grounded = false;
    }
}
