﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire_Move : MonoBehaviour
{
    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
        rb.velocity = transform.right * -40;
        StartCoroutine(PrintfAfter(0.5f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
        IEnumerator PrintfAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Destroy(gameObject);
    }
}
