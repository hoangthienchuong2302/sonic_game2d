﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    // Start is called before the first frame update
    public int faceVal = -1;
    bool facingRight;
    public GameObject pickupPrelab;
    public GameObject fireBallBoss;
    public GameObject panelWin;
    Rigidbody2D myBody;
    public Animator myAnim;
    public Transform firePoint;
    public Animator myAnim2;
    public bool Die = false;
    public int count = 0;
    // Start is called before the first frame update
    void Awake()
    {
        // 
        myBody = gameObject.GetComponent<Rigidbody2D>();
        myAnim = gameObject.GetComponent<Animator>();
        // 
        Instantiate(fireBallBoss, firePoint.position, firePoint.rotation);
        StartCoroutine(FireAfter(1f));
        if (!Die)
            StartCoroutine(PrintfAfter(4));
    }

    // Update is called once per frame
    void Update()
    {
        if (!Die)
            transform.Translate(Time.deltaTime * 8 * -1, 0, 0);


    }
    IEnumerator PrintfAfter(float seconds)
    {

        yield return new WaitForSeconds(seconds);
        faceVal = faceVal * -1;
        transform.Rotate(Vector3.up * 180 * -faceVal);
        StartCoroutine(PrintfAfter(4));
    }
    void OnTriggerEnter2D(Collider2D Colli)
    {

        if (Colli.gameObject.tag == "FireBall")
        {
            count++;
            if (count == 1)
            {
                myAnim2.SetBool("hit1", true);
            }
            if (count == 2)
            {
                myAnim2.SetBool("hit2", true);
            }
            if (count == 3)
            {
                myAnim2.SetBool("hit3", true);
            }
            if (count == 4)
            {
                Destroy(gameObject);
                Instantiate(pickupPrelab, transform.position, Quaternion.identity);
                panelWin.SetActive(true);
            }
        }
    }
    IEnumerator FireAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Instantiate(fireBallBoss, firePoint.position, firePoint.rotation);
        StartCoroutine(FireAfter(1f));

    }

    void _flip(int val)
    {
        facingRight = !facingRight;
        transform.Rotate(Vector3.up * 180 * val);
    }
}
